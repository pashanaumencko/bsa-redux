import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';
import { sendMessageAction } from '../../actions/actions';
import moment from 'moment';
import { getUniqueId } from '../../services/services';

import './MessageInput.css';


class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    let inputMessage = event.target.elements.messageInput;
    const newMessage = {
      user: 'Pavlo',
      id: getUniqueId(),
      created_at: moment().format('YYYY-MM-DD HH:mm'),
      message: inputMessage.value
    };
    if(inputMessage.value) {
      this.props.sendMessageAction(newMessage);
    }
    inputMessage.value = '';
  } 

  render() {
    return(
      <div className="type-msg">
        <form className="input-msg-write" onSubmit={this.onSubmit}>
          <textarea type="text" className="write-msg" name="messageInput" placeholder="Type a message" />
          <button className="msg-send-btn" type="submit"><i className="fa fa-paper-plane-o" aria-hidden="true"></i></button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  ...actions,
  sendMessageAction
};


export default connect(null, mapDispatchToProps)(MessageInput);