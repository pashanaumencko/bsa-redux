import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/actions';
import { deleteMessageAction, showPageAction } from '../../actions/actions';

import './MyMessage.css';

class MyMessage extends React.Component {
  constructor(props) {
    super(props);
    this.onDeleteClick = this.onDeleteClick.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
  }

  onDeleteClick(event, id) {
    this.props.deleteMessageAction(id);
  }

  onEditClick(event) {
    const message = this.props.message;
    this.props.showPageAction();
    this.props.setEditedMessageAction(message);
  }

  render() {
    const { id:messageId, user:userName, message:messageText, created_at:messageDate } = this.props.message;
    return(
      <div className="outgoing-msg">
        <div className="sent-msg">
          <p>
            <h6>{userName}</h6>
            {messageText}
          </p>
          <div className="msg-info">
            <span className="time-date">{messageDate}</span>
            <div className="msg-actions">
              <div className="msg-delete" onClick={event => this.onDeleteClick(event, messageId)}>
                <i className="fa fa-trash"></i>
              </div>
              <div className="msg-edit" onClick={this.onEditClick}>
                <i className="fa fa-edit"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    messages: state.messages,
  };
};

const mapDispatchToProps = {
  ...actions,
  deleteMessageAction,
  showPageAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MyMessage);