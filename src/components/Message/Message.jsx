import React from 'react';

import './Message.css';


class Message extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      likes: 0,
      updated: false
    };

    this.onLikeClick = this.onLikeClick.bind(this);
  }

  onLikeClick() {
    if(!this.state.updated) {
      this.setState((prevState) => {
        return {
          likes: prevState.likes + 1,
          updated: true
        };
      });
    } else {

      this.setState((prevState) => {
        return {
          likes: prevState.likes - 1,
          updated: false
        };
      });
    }
  }

  render() {
    const { avatar:avatarSrc, user:userName, message:messageText, created_at:messageDate } = this.props.message;

    return(
      <div className="incoming-msg">
        <div className="incoming-msg-img"> <img src={avatarSrc} width="50" height="50" alt="avatar" /> </div>
        <div className="received-msg">
          <div className="received-withd-msg">
            <p>
              <h6>{userName}</h6>
              {messageText}
            </p>
            <div className="msg-info">
              <span className="time-date">{messageDate}</span>
              <div className="msg-likes" onClick={this.onLikeClick}>{this.state.likes}<i className="fa fa-heart"></i></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Message;