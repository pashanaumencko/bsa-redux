import { 
  GET_DATA,
  SEND_MESSAGE, 
  EDIT_MESSAGE, 
  DELETE_MESSAGE, 
  SET_EDIT_MESSAGE, 
  SHOW_PAGE, 
  HIDE_PAGE 
} from '../constants/ActionsTypes';

const getMessagesAction = (messages) => ({
  type: GET_DATA,
  messages
});

const sendMessageAction = (message) => ({
  type: SEND_MESSAGE,
  message
});

const editMessageAction = (message) => ({
  type: EDIT_MESSAGE,
  message
});

const deleteMessageAction = (id) => ({
  type: DELETE_MESSAGE,
  id
});

const setEditedMessageAction = (message) => ({
  type: SET_EDIT_MESSAGE,
  message
});

const showPageAction = () => ({
  type: SHOW_PAGE
});

const hidePageAction = () => ({
  type: HIDE_PAGE
});

export { 
  getMessagesAction, 
  sendMessageAction, 
  editMessageAction, 
  deleteMessageAction, 
  setEditedMessageAction, 
  showPageAction, 
  hidePageAction,
};