import { createStore, applyMiddleware } from "redux";
import ChatReducer from "./reducer/index";
import thunk from 'redux-thunk';

export default createStore(ChatReducer, applyMiddleware(thunk));